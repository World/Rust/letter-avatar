pub mod generate {
    use cairo;
    use cairo::IoError as Error;
    use pango;
    use pangocairo;
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    use unicode_segmentation::UnicodeSegmentation;

    struct Color {
        r: i32,
        g: i32,
        b: i32,
    }

    fn calculate_hash<T: Hash>(t: &T) -> u64 {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }

    fn get_initials(name: String) -> Result<String, Error> {
        let mut words = name.unicode_words();
        let first = words
            .next()
            .and_then(|w| UnicodeSegmentation::graphemes(w, true).next())
            .unwrap_or_default();
        let second = words
            .next()
            .and_then(|w| UnicodeSegmentation::graphemes(w, true).next())
            .unwrap_or_default();
        let initials = format!("{}{}", first, second);

        Ok(initials.to_uppercase())
    }

    pub fn new(uid: String, name: Option<String>, size: f64) -> Result<cairo::ImageSurface, Error> {
        // Our color palette with a darker and a muted variant for each one
        let colors = [
            [
                Color {
                    r: 206,
                    g: 77,
                    b: 205,
                },
                Color {
                    r: 251,
                    g: 224,
                    b: 251,
                },
            ],
            [
                Color {
                    r: 121,
                    g: 81,
                    b: 192,
                },
                Color {
                    r: 231,
                    g: 218,
                    b: 251,
                },
            ],
            [
                Color {
                    r: 78,
                    g: 99,
                    b: 201,
                },
                Color {
                    r: 207,
                    g: 215,
                    b: 248,
                },
            ],
            [
                Color {
                    r: 66,
                    g: 160,
                    b: 243,
                },
                Color {
                    r: 214,
                    g: 234,
                    b: 252,
                },
            ],
            [
                Color {
                    r: 70,
                    g: 189,
                    b: 158,
                },
                Color {
                    r: 212,
                    g: 248,
                    b: 239,
                },
            ],
            [
                Color {
                    r: 117,
                    g: 184,
                    b: 45,
                },
                Color {
                    r: 220,
                    g: 247,
                    b: 191,
                },
            ],
            [
                Color {
                    r: 235,
                    g: 121,
                    b: 10,
                },
                Color {
                    r: 254,
                    g: 235,
                    b: 218,
                },
            ],
            [
                Color {
                    r: 227,
                    g: 61,
                    b: 34,
                },
                Color {
                    r: 251,
                    g: 219,
                    b: 211,
                },
            ],
            [
                Color {
                    r: 109,
                    g: 109,
                    b: 109,
                },
                Color {
                    r: 219,
                    g: 219,
                    b: 219,
                },
            ],
        ];

        //let fname = cache_path(fname)?;

        let image = cairo::ImageSurface::create(cairo::Format::ARgb32, size as i32, size as i32)?;
        let g = cairo::Context::new(&image).unwrap();

        let color_index = calculate_hash(&uid) as usize % colors.len() as usize;
        let bg_c = &colors[color_index][0];
        g.set_source_rgba(
            bg_c.r as f64 / 256.,
            bg_c.g as f64 / 256.,
            bg_c.b as f64 / 256.,
            1.,
        );

        g.arc(
            size / 2f64,
            size / 2f64,
            size / 2f64,
            0.0,
            2.0 * std::f64::consts::PI,
        );
        g.fill().unwrap();

        let fg_c = &colors[color_index][1];
        g.set_source_rgba(
            fg_c.r as f64 / 256.,
            fg_c.g as f64 / 256.,
            fg_c.b as f64 / 256.,
            1.,
        );

        /* If we don't have the username, we create just a empty colored circle */
        if let Some(name) = name {
            if !name.is_empty() {
                let initials = get_initials(name)?;

                let layout = pangocairo::functions::create_layout(&g).unwrap();
                let fontdesc = if size < 17f64 {
                    pango::FontDescription::from_string("Cantarell Ultra-Bold 5")
                } else if size < 25f64 {
                    pango::FontDescription::from_string("Cantarell Ultra-Bold 9")
                } else if size < 31f64 {
                    pango::FontDescription::from_string("Cantarell Ultra-Bold 11")
                } else if size < 41f64 {
                    pango::FontDescription::from_string("Cantarell Ultra-Bold 13")
                } else {
                    pango::FontDescription::from_string("Cantarell Ultra-Bold 18")
                };
                layout.set_font_description(Some(&fontdesc));
                layout.set_text(&initials);
                // Move to center of the background shape we drew,
                // offset by half the size of the glyph
                let bx = image.width();
                let by = image.height();
                let (ox, oy) = layout.pixel_size();
                g.translate((bx - ox) as f64 / 2., (by - oy) as f64 / 2.);
                // Finally draw the glyph
                pangocairo::functions::show_layout(&g, &layout);
            }
        }

        Ok(image)
    }
}

mod tests {
    #[test]
    fn create_surface() {
        let result = super::generate::new(
            String::from("jsparber"),
            Some(String::from("Julian Sparber")),
            50f64,
        );
        assert!(result.is_ok());
    }
}
